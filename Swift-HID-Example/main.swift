//
//  main.swift
//
//  Created by Eric Betts on 6/19/15.
//  Copyright © 2015 Eric Betts. All rights reserved.
//

import Foundation
import AppKit

let starlink = Starlink.singleton
let center = NotificationCenter.default
let mainQueue = OperationQueue.main

// Kick off the state machine
center.addObserver(forName: Notification.Name("deviceConnected"), object: nil, queue: mainQueue) { (note) in
    
    center.addObserver(forName: Notification.Name("input"), object: nil, queue: mainQueue) { (note) in
        guard let userInfo = note.userInfo else {
            print("No userInfo")
            return
        }
        
        guard let message = userInfo["message"] else {
            print("No message in userInfo")
            return
        }
        
        guard let data = message as? Data else {
            print("Message in not Data")
            return
        }
        
        print(data)
    }
    
    starlink.output(Data([0x02, 0x00, 0x00, 0x00, 0x00]))
}

starlink.initUsb()
