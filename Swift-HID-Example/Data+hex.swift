//
//  Data+hex.swift
//  expecimal
//
//  Created by Eric Betts on 1/29/18.
//  Copyright © 2018 Eric Betts. All rights reserved.
//

import Foundation
extension Data {
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCase = HexEncodingOptions(rawValue: 1 << 0)
        static let midcate = HexEncodingOptions(rawValue: 1 << 1)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCase) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}
